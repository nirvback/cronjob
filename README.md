# cronjob

Minimalistic docker container that runs cron job

Example usage:
```
docker container run -d -e 'CRON_STRINGS=*/15 * * * * curl https://example.com/wp-cron.php' nirvback/cronjob
```

Simalarly with docker compose/docker stack:
```
  wpcron:
    image: nirvback/cronjob:latest
    deploy:
      replicas: 1
    environment:
      CRON_STRINGS: '*/5 * * * * curl -v https://www.example.com/wp-cron.php? >/tmp/example.com.out 2>/tmp/example.com.err\n* * * * * curl -v http://example.net/ >/tmp/example.net.out 2>/tmp/example.net.err'
```
