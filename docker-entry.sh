#!/bin/sh
set -e

rm -rf /var/spool/cron/crontabs && mkdir -m 0644 -p /var/spool/cron/crontabs

if [ ! -z "$CRON_STRINGS" ]; then
  echo -e "$CRON_STRINGS\n" > /var/spool/cron/crontabs/CRON_STRINGS
else
  echo "You need to define the cronjobs you want to run in \$CRON_STRINGS (separated by \\n)."
  exit 1
fi

chmod -R 0644 /var/spool/cron/crontabs

exec "$@"
