#!/bin/sh
set -e

# crond running in background and log file reading every second by tail to STDOUT
crond -s /var/spool/cron/crontabs -b -L /var/log/cron/cron.log "$@" && tail -f /var/log/cron/cron.log
